/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.modules.bootstrapui.elements;

import lombok.NonNull;

/**
 * Lists the standard Bootstrap GlyphIcons.
 *
 * @author Arne Vandamme
 */
public class GlyphIcon extends IconViewElement
{
	public static final String ASTERISK = "glyphicon-asterisk";
	public static final String PLUS = "glyphicon-plus";
	public static final String EURO = "glyphicon-euro";
	public static final String EUR = "glyphicon-eur";
	public static final String MINUS = "glyphicon-minus";
	public static final String CLOUD = "glyphicon-cloud";
	public static final String ENVELOPE = "glyphicon-envelope";
	public static final String PENCIL = "glyphicon-pencil";
	public static final String GLASS = "glyphicon-glass";
	public static final String MUSIC = "glyphicon-music";
	public static final String SEARCH = "glyphicon-search";
	public static final String HEART = "glyphicon-heart";
	public static final String STAR = "glyphicon-star";
	public static final String STAR_EMPTY = "glyphicon-star-empty";
	public static final String USER = "glyphicon-user";
	public static final String FILM = "glyphicon-film";
	public static final String TH_LARGE = "glyphicon-th-large";
	public static final String TH = "glyphicon-th";
	public static final String TH_LIST = "glyphicon-th-list";
	public static final String OK = "glyphicon-ok";
	public static final String REMOVE = "glyphicon-remove";
	public static final String ZOOM_IN = "glyphicon-zoom-in";
	public static final String ZOOM_OUT = "glyphicon-zoom-out";
	public static final String OFF = "glyphicon-off";
	public static final String SIGNAL = "glyphicon-signal";
	public static final String COG = "glyphicon-cog";
	public static final String TRASH = "glyphicon-trash";
	public static final String HOME = "glyphicon-home";
	public static final String FILE = "glyphicon-file";
	public static final String TIME = "glyphicon-time";
	public static final String ROAD = "glyphicon-road";
	public static final String DOWNLOAD_ALT = "glyphicon-download-alt";
	public static final String DOWNLOAD = "glyphicon-download";
	public static final String UPLOAD = "glyphicon-upload";
	public static final String INBOX = "glyphicon-inbox";
	public static final String PLAY_CIRCLE = "glyphicon-play-circle";
	public static final String REPEAT = "glyphicon-repeat";
	public static final String REFRESH = "glyphicon-refresh";
	public static final String LIST_ALT = "glyphicon-list-alt";
	public static final String LOCK = "glyphicon-lock";
	public static final String FLAG = "glyphicon-flag";
	public static final String HEADPHONES = "glyphicon-headphones";
	public static final String VOLUME_OFF = "glyphicon-volume-off";
	public static final String VOLUME_DOWN = "glyphicon-volume-down";
	public static final String VOLUME_UP = "glyphicon-volume-up";
	public static final String QRCODE = "glyphicon-qrcode";
	public static final String BARCODE = "glyphicon-barcode";
	public static final String TAG = "glyphicon-tag";
	public static final String TAGS = "glyphicon-tags";
	public static final String BOOK = "glyphicon-book";
	public static final String BOOKMARK = "glyphicon-bookmark";
	public static final String PRINT = "glyphicon-print";
	public static final String CAMERA = "glyphicon-camera";
	public static final String FONT = "glyphicon-font";
	public static final String BOLD = "glyphicon-bold";
	public static final String ITALIC = "glyphicon-italic";
	public static final String TEXT_HEIGHT = "glyphicon-text-height";
	public static final String TEXT_WIDTH = "glyphicon-text-width";
	public static final String ALIGN_LEFT = "glyphicon-align-left";
	public static final String ALIGN_CENTER = "glyphicon-align-center";
	public static final String ALIGN_RIGHT = "glyphicon-align-right";
	public static final String ALIGN_JUSTIFY = "glyphicon-align-justify";
	public static final String LIST = "glyphicon-list";
	public static final String INDENT_LEFT = "glyphicon-indent-left";
	public static final String INDENT_RIGHT = "glyphicon-indent-right";
	public static final String FACETIME_VIDEO = "glyphicon-facetime-video";
	public static final String PICTURE = "glyphicon-picture";
	public static final String MAP_MARKER = "glyphicon-map-marker";
	public static final String ADJUST = "glyphicon-adjust";
	public static final String TINT = "glyphicon-tint";
	public static final String EDIT = "glyphicon-edit";
	public static final String SHARE = "glyphicon-share";
	public static final String CHECK = "glyphicon-check";
	public static final String MOVE = "glyphicon-move";
	public static final String STEP_BACKWARD = "glyphicon-step-backward";
	public static final String FAST_BACKWARD = "glyphicon-fast-backward";
	public static final String BACKWARD = "glyphicon-backward";
	public static final String PLAY = "glyphicon-play";
	public static final String PAUSE = "glyphicon-pause";
	public static final String STOP = "glyphicon-stop";
	public static final String FORWARD = "glyphicon-forward";
	public static final String FAST_FORWARD = "glyphicon-fast-forward";
	public static final String STEP_FORWARD = "glyphicon-step-forward";
	public static final String EJECT = "glyphicon-eject";
	public static final String CHEVRON_LEFT = "glyphicon-chevron-left";
	public static final String CHEVRON_RIGHT = "glyphicon-chevron-right";
	public static final String PLUS_SIGN = "glyphicon-plus-sign";
	public static final String MINUS_SIGN = "glyphicon-minus-sign";
	public static final String REMOVE_SIGN = "glyphicon-remove-sign";
	public static final String OK_SIGN = "glyphicon-ok-sign";
	public static final String QUESTION_SIGN = "glyphicon-question-sign";
	public static final String INFO_SIGN = "glyphicon-info-sign";
	public static final String SCREENSHOT = "glyphicon-screenshot";
	public static final String REMOVE_CIRCLE = "glyphicon-remove-circle";
	public static final String OK_CIRCLE = "glyphicon-ok-circle";
	public static final String BAN_CIRCLE = "glyphicon-ban-circle";
	public static final String ARROW_LEFT = "glyphicon-arrow-left";
	public static final String ARROW_RIGHT = "glyphicon-arrow-right";
	public static final String ARROW_UP = "glyphicon-arrow-up";
	public static final String ARROW_DOWN = "glyphicon-arrow-down";
	public static final String SHARE_ALT = "glyphicon-share-alt";
	public static final String RESIZE_FULL = "glyphicon-resize-full";
	public static final String RESIZE_SMALL = "glyphicon-resize-small";
	public static final String EXCLAMATION_SIGN = "glyphicon-exclamation-sign";
	public static final String GIFT = "glyphicon-gift";
	public static final String LEAF = "glyphicon-leaf";
	public static final String FIRE = "glyphicon-fire";
	public static final String EYE_OPEN = "glyphicon-eye-open";
	public static final String EYE_CLOSE = "glyphicon-eye-close";
	public static final String WARNING_SIGN = "glyphicon-warning-sign";
	public static final String PLANE = "glyphicon-plane";
	public static final String CALENDAR = "glyphicon-calendar";
	public static final String RANDOM = "glyphicon-random";
	public static final String COMMENT = "glyphicon-comment";
	public static final String MAGNET = "glyphicon-magnet";
	public static final String CHEVRON_UP = "glyphicon-chevron-up";
	public static final String CHEVRON_DOWN = "glyphicon-chevron-down";
	public static final String RETWEET = "glyphicon-retweet";
	public static final String SHOPPING_CART = "glyphicon-shopping-cart";
	public static final String FOLDER_CLOSE = "glyphicon-folder-close";
	public static final String FOLDER_OPEN = "glyphicon-folder-open";
	public static final String RESIZE_VERTICAL = "glyphicon-resize-vertical";
	public static final String RESIZE_HORIZONTAL = "glyphicon-resize-horizontal";
	public static final String HDD = "glyphicon-hdd";
	public static final String BULLHORN = "glyphicon-bullhorn";
	public static final String BELL = "glyphicon-bell";
	public static final String CERTIFICATE = "glyphicon-certificate";
	public static final String THUMBS_UP = "glyphicon-thumbs-up";
	public static final String THUMBS_DOWN = "glyphicon-thumbs-down";
	public static final String HAND_RIGHT = "glyphicon-hand-right";
	public static final String HAND_LEFT = "glyphicon-hand-left";
	public static final String HAND_UP = "glyphicon-hand-up";
	public static final String HAND_DOWN = "glyphicon-hand-down";
	public static final String CIRCLE_ARROW_RIGHT = "glyphicon-circle-arrow-right";
	public static final String CIRCLE_ARROW_LEFT = "glyphicon-circle-arrow-left";
	public static final String CIRCLE_ARROW_UP = "glyphicon-circle-arrow-up";
	public static final String CIRCLE_ARROW_DOWN = "glyphicon-circle-arrow-down";
	public static final String GLOBE = "glyphicon-globe";
	public static final String WRENCH = "glyphicon-wrench";
	public static final String TASKS = "glyphicon-tasks";
	public static final String FILTER = "glyphicon-filter";
	public static final String BRIEFCASE = "glyphicon-briefcase";
	public static final String FULLSCREEN = "glyphicon-fullscreen";
	public static final String DASHBOARD = "glyphicon-dashboard";
	public static final String PAPERCLIP = "glyphicon-paperclip";
	public static final String HEART_EMPTY = "glyphicon-heart-empty";
	public static final String LINK = "glyphicon-link";
	public static final String PHONE = "glyphicon-phone";
	public static final String PUSHPIN = "glyphicon-pushpin";
	public static final String USD = "glyphicon-usd";
	public static final String GBP = "glyphicon-gbp";
	public static final String SORT = "glyphicon-sort";
	public static final String SORT_BY_ALPHABET = "glyphicon-sort-by-alphabet";
	public static final String SORT_BY_ALPHABET_ALT = "glyphicon-sort-by-alphabet-alt";
	public static final String SORT_BY_ORDER = "glyphicon-sort-by-order";
	public static final String SORT_BY_ORDER_ALT = "glyphicon-sort-by-order-alt";
	public static final String SORT_BY_ATTRIBUTES = "glyphicon-sort-by-attributes";
	public static final String SORT_BY_ATTRIBUTES_ALT = "glyphicon-sort-by-attributes-alt";
	public static final String UNCHECKED = "glyphicon-unchecked";
	public static final String EXPAND = "glyphicon-expand";
	public static final String COLLAPSE_DOWN = "glyphicon-collapse-down";
	public static final String COLLAPSE_UP = "glyphicon-collapse-up";
	public static final String LOG_IN = "glyphicon-log-in";
	public static final String FLASH = "glyphicon-flash";
	public static final String LOG_OUT = "glyphicon-log-out";
	public static final String NEW_WINDOW = "glyphicon-new-window";
	public static final String RECORD = "glyphicon-record";
	public static final String SAVE = "glyphicon-save";
	public static final String OPEN = "glyphicon-open";
	public static final String SAVED = "glyphicon-saved";
	public static final String IMPORT = "glyphicon-import";
	public static final String EXPORT = "glyphicon-export";
	public static final String SEND = "glyphicon-send";
	public static final String FLOPPY_DISK = "glyphicon-floppy-disk";
	public static final String FLOPPY_SAVED = "glyphicon-floppy-saved";
	public static final String FLOPPY_REMOVE = "glyphicon-floppy-remove";
	public static final String FLOPPY_SAVE = "glyphicon-floppy-save";
	public static final String FLOPPY_OPEN = "glyphicon-floppy-open";
	public static final String CREDIT_CARD = "glyphicon-credit-card";
	public static final String TRANSFER = "glyphicon-transfer";
	public static final String CUTLERY = "glyphicon-cutlery";
	public static final String HEADER = "glyphicon-header";
	public static final String COMPRESSED = "glyphicon-compressed";
	public static final String EARPHONE = "glyphicon-earphone";
	public static final String PHONE_ALT = "glyphicon-phone-alt";
	public static final String TOWER = "glyphicon-tower";
	public static final String STATS = "glyphicon-stats";
	public static final String SD_VIDEO = "glyphicon-sd-video";
	public static final String HD_VIDEO = "glyphicon-hd-video";
	public static final String SUBTITLES = "glyphicon-subtitles";
	public static final String SOUND_STEREO = "glyphicon-sound-stereo";
	public static final String SOUND_DOLBY = "glyphicon-sound-dolby";
	public static final String SOUND_5_1 = "glyphicon-sound-5-1";
	public static final String SOUND_6_1 = "glyphicon-sound-6-1";
	public static final String SOUND_7_1 = "glyphicon-sound-7-1";
	public static final String COPYRIGHT_MARK = "glyphicon-copyright-mark";
	public static final String REGISTRATION_MARK = "glyphicon-registration-mark";
	public static final String CLOUD_DOWNLOAD = "glyphicon-cloud-download";
	public static final String CLOUD_UPLOAD = "glyphicon-cloud-upload";
	public static final String TREE_CONIFER = "glyphicon-tree-conifer";
	public static final String TREE_DECIDUOUS = "glyphicon-tree-deciduous";
	public static final String CD = "glyphicon-cd";
	public static final String SAVE_FILE = "glyphicon-save-file";
	public static final String OPEN_FILE = "glyphicon-open-file";
	public static final String LEVEL_UP = "glyphicon-level-up";
	public static final String COPY = "glyphicon-copy";
	public static final String PASTE = "glyphicon-paste";
	public static final String ALERT = "glyphicon-alert";
	public static final String EQUALIZER = "glyphicon-equalizer";
	public static final String KING = "glyphicon-king";
	public static final String QUEEN = "glyphicon-queen";
	public static final String PAWN = "glyphicon-pawn";
	public static final String BISHOP = "glyphicon-bishop";
	public static final String KNIGHT = "glyphicon-knight";
	public static final String BABY_FORMULA = "glyphicon-baby-formula";
	public static final String TENT = "glyphicon-tent";
	public static final String BLACKBOARD = "glyphicon-blackboard";
	public static final String BED = "glyphicon-bed";
	public static final String APPLE = "glyphicon-apple";
	public static final String ERASE = "glyphicon-erase";
	public static final String HOURGLASS = "glyphicon-hourglass";
	public static final String LAMP = "glyphicon-lamp";
	public static final String DUPLICATE = "glyphicon-duplicate";
	public static final String PIGGY_BANK = "glyphicon-piggy-bank";
	public static final String SCISSORS = "glyphicon-scissors";
	public static final String BITCOIN = "glyphicon-bitcoin";
	public static final String YEN = "glyphicon-yen";
	public static final String RUBLE = "glyphicon-ruble";
	public static final String SCALE = "glyphicon-scale";
	public static final String ICE_LOLLY = "glyphicon-ice-lolly";
	public static final String ICE_LOLLY_TASTED = "glyphicon-ice-lolly-tasted";
	public static final String EDUCATION = "glyphicon-education";
	public static final String OPTION_HORIZONTAL = "glyphicon-option-horizontal";
	public static final String OPTION_VERTICAL = "glyphicon-option-vertical";
	public static final String MENU_HAMBURGER = "glyphicon-menu-hamburger";
	public static final String MODAL_WINDOW = "glyphicon-modal-window";
	public static final String OIL = "glyphicon-oil";
	public static final String GRAIN = "glyphicon-grain";
	public static final String SUNGLASSES = "glyphicon-sunglasses";
	public static final String TEXT_SIZE = "glyphicon-text-size";
	public static final String TEXT_COLOR = "glyphicon-text-color";
	public static final String TEXT_BACKGROUND = "glyphicon-text-background";
	public static final String OBJECT_ALIGN_TOP = "glyphicon-object-align-top";
	public static final String OBJECT_ALIGN_BOTTOM = "glyphicon-object-align-bottom";
	public static final String OBJECT_ALIGN_HORIZONTAL = "glyphicon-object-align-horizontal";
	public static final String OBJECT_ALIGN_LEFT = "glyphicon-object-align-left";
	public static final String OBJECT_ALIGN_VERTICAL = "glyphicon-object-align-vertical";
	public static final String OBJECT_ALIGN_RIGHT = "glyphicon-object-align-right";
	public static final String TRIANGLE_RIGHT = "glyphicon-triangle-right";
	public static final String TRIANGLE_LEFT = "glyphicon-triangle-left";
	public static final String TRIANGLE_BOTTOM = "glyphicon-triangle-bottom";
	public static final String TRIANGLE_TOP = "glyphicon-triangle-top";
	public static final String CONSOLE = "glyphicon-console";
	public static final String SUPERSCRIPT = "glyphicon-superscript";
	public static final String SUBSCRIPT = "glyphicon-subscript";
	public static final String MENU_LEFT = "glyphicon-menu-left";
	public static final String MENU_RIGHT = "glyphicon-menu-right";
	public static final String MENU_DOWN = "glyphicon-menu-down";
	public static final String MENU_UP = "glyphicon-menu-up";

	private String glyph;

	public GlyphIcon() {
	}

	public GlyphIcon( String glyph ) {
		setGlyph( glyph );
	}

	public void setGlyph( @NonNull String glyph ) {
		this.glyph = glyph;
		setIconCss( "glyphicon " + glyph );
	}

	public String getGlyph() {
		return glyph;
	}
}
