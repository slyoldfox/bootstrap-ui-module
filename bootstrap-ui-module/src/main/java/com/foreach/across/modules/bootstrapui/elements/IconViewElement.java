/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.modules.bootstrapui.elements;

import com.foreach.across.modules.web.ui.elements.AbstractVoidNodeViewElement;

/**
 * Represents a simple icon element that does not support child elements.
 *
 * @author Arne Vandamme
 * @see GlyphIcon
 */
public abstract class IconViewElement extends AbstractVoidNodeViewElement
{
	public static final String ELEMENT_TYPE = BootstrapUiElements.ICON;

	private String iconCss;

	public IconViewElement() {
		super( "span" );
		setElementType( ELEMENT_TYPE );
		setAttribute( "aria-hidden", "true" );
	}

	@Override
	public String getTagName() {
		return super.getTagName();
	}

	@Override
	public void setTagName( String tagName ) {
		super.setTagName( tagName );
	}

	public String getIconCss() {
		return iconCss;
	}

	protected void setIconCss( String iconCss ) {
		this.iconCss = iconCss;
	}
}
