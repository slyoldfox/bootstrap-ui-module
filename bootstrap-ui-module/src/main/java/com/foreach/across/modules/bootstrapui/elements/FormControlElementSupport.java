/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.modules.bootstrapui.elements;

import com.foreach.across.modules.web.ui.elements.AbstractNodeViewElement;

/**
 * @author Arne Vandamme
 */
public abstract class FormControlElementSupport extends AbstractNodeViewElement implements FormControlElement
{
	private boolean disabled, readonly, required;
	private String controlName;

	private boolean htmlIdSpecified;

	protected FormControlElementSupport( String elementType ) {
		super( "input" );
		setElementType( elementType );
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled( boolean disabled ) {
		this.disabled = disabled;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly( boolean readonly ) {
		this.readonly = readonly;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired( boolean required ) {
		this.required = required;
	}

	@Override
	public void setName( String name ) {
		super.setName( name );
		if ( controlName == null ) {
			setControlName( name );
		}
	}

	@Override
	public String getControlName() {
		return controlName;
	}

	@Override
	public void setControlName( String controlName ) {
		this.controlName = controlName;
		if ( !htmlIdSpecified ) {
			super.setHtmlId( controlName );
		}
	}

	@Override
	public void setHtmlId( String htmlId ) {
		this.htmlIdSpecified = true;
		super.setHtmlId( htmlId );
	}
}
